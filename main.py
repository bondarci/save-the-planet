#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame
import sys
from house import House
from settings import Settings
from car import Car



def main():

    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("Save-The-Planet")
    house = House(screen)
    car = Car(screen)
    bg_color = (230, 0, 0)




    while True:
        for event in pygame.event.get():
            screen.fill(ai_settings.bg_color)
            house.blitme()
            car.blitme()
            if event.type == pygame.QUIT:
                sys.exit



        pygame.display.flip()


main()
    
